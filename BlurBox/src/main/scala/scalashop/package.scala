
import common._

package object scalashop {

  /** The value of every pixel is represented as a 32 bit integer. */
  type RGBA = Int

  /** Returns the red component. */
  def red(c: RGBA): Int = (0xff000000 & c) >>> 24

  /** Returns the green component. */
  def green(c: RGBA): Int = (0x00ff0000 & c) >>> 16

  /** Returns the blue component. */
  def blue(c: RGBA): Int = (0x0000ff00 & c) >>> 8

  /** Returns the alpha component. */
  def alpha(c: RGBA): Int = (0x000000ff & c) >>> 0

  /** Used to create an RGBA value from separate components. */
  def rgba(r: Int, g: Int, b: Int, a: Int): RGBA = {
    (r << 24) | (g << 16) | (b << 8) | (a << 0)
  }

  /** Restricts the integer into the specified range. */
  def clamp(v: Int, min: Int, max: Int): Int = {
    if (v < min) min
    else if (v > max) max
    else v
  }

  /** Image is a two-dimensional matrix of pixel values. */
  class Img(val width: Int, val height: Int, private val data: Array[RGBA]) {
    def this(w: Int, h: Int) = this(w, h, new Array(w * h))
    def apply(x: Int, y: Int): RGBA = data(y * width + x)
    def update(x: Int, y: Int, c: RGBA): Unit = data(y * width + x) = c
  }

  /** Computes the blurred RGBA value of a single pixel of the input image. */
  def boxBlurKernel(src: Img, x: Int, y: Int, radius: Int): RGBA = {

    var redBlur, greenBlur, blueBur, alphaBlur, count = 0 // Initialize vars

    var provX = clamp(x - radius, 0, src.width - 1) // Beginning of blur area on x axis
    while (provX <= clamp(x + radius, 0, src.width - 1)) {
      var provY = clamp(y - radius, 0, src.height - 1) // Beginning of blur area on y axis
      while (provY <= clamp(y + radius, 0, src.height - 1)) {
        val pixelColor = src.apply(provX, provY) // Get color of pixel

        // Extract color component and add them to the sum

        redBlur += red(pixelColor)
        greenBlur += green(pixelColor)
        blueBur += blue(pixelColor)
        alphaBlur += alpha(pixelColor)

        count += 1 // Increase total of pixels used

        provY += 1
      }
      provX += 1
    }
    rgba(redBlur / count , greenBlur / count, blueBur / count, alphaBlur / count) // Return new pixel color
  }

}
