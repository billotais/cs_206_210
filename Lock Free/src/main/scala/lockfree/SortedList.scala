package lockfree

import scala.annotation.tailrec

class SortedList extends AbstractSortedList {

  // The sentinel node at the head.
  private val _head = createNode(0, None, isHead = true)

  // The first logical node is referenced by the head.
  def firstNode: Option[Node] = _head.next

  // Finds the first node whose value satisfies the predicate.
  // Returns the predecessor of the node and the node.
  def findNodeWithPrev(pred: Int => Boolean): (Node, Option[Node]) = {

    var prev: Node = _head
    var next: Option[Node] = firstNode

    while (next.isDefined && (!pred(next.get.value) || next.get.deleted)) {
      if (next.get.deleted) {
        prev.atomicState.compareAndSet((next, false), (next.get.next, false))
        findNodeWithPrev(pred)
      }
      prev = next.get
      next = prev.next
    }
    (prev, next)
  }

  // Insert an element in the list.

  def insert(e: Int): Unit = {
    val (prev, next) = findNodeWithPrev(_ >= e)
    val newNode = createNode(e, next)
    if (!prev.atomicState.compareAndSet((next, false), (Some(newNode), false))) insert(e)
  }

  // Checks if the list contains an element.
  def contains(e: Int): Boolean = {
    val node = findNodeWithPrev(_ == e)._2
    node.isDefined
  }

  // Delete an element from the list.
  // Should only delete one element when multiple occurences are present.
  def delete(e: Int): Boolean = {
    if (!contains(e)) false
    else {

      val (prev, next) = findNodeWithPrev(_ == e)
      val canMark = next.get.mark
      if (!canMark) delete(e)
      true

    }
  }
}
