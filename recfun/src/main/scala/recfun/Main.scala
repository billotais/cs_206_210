package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
    
    
    
  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int = 
    	if (c == 0 || c == r) 1
    	else pascal(c - 1, r - 1) + pascal(c, r - 1)
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {
    	
    	def intForChar(c: Char): Int = 
    		if (c == '(') 1 
    		else if (c == ')') -1
    		else 0
    		
    	def balanceIter(provList: List[Char], count: Int): Boolean =
    		if (provList.isEmpty) (count == 0)
    		else if (count < 0) false
    		else balanceIter(provList.tail, count + intForChar(provList.head))
    	
    	balanceIter(chars, 0)
    
    }
  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = 
    {	
    	def changeIter(money: Int, coins: List[Int], sum: Int): Int = 
    		if (sum == 0) 1
    		else if (sum < 0) 0
    		else {
    			var a = 0
    			for (c ← coins){
    				if(c <= money){
    					a = a + changeIter(c, coins, sum-c)
    				}	
    			}
    			a
    		}	
    	changeIter(money, coins, money)
    } 	
  }
