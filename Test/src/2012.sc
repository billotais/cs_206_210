val l = List("Salut", "Salut", "les", "gens", "les", "Salut")
l.groupBy(x => x)
def insertion[A](x:A, xs:List[A]): List[List[A]] = xs match {
  case Nil => List(List(x))
  case y :: ys => (x :: y :: ys) :: (insertion(x, ys) map (y :: _))
}
insertion(2, List(3, 4, 5))

def permutation[A](as:List[A]): List[List[A]] = as match {
  case Nil => List(Nil)
  case b :: bs => permutation(bs) flatMap (insertion(b, _))
}
permutation(List(1, 2, 3))