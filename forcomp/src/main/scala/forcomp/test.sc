package forcomp
import Anagrams._
object test {
def combinations(occurrences: Occurrences): List[Occurrences] =
  {
  	def iter(provOcc: Occurrences): List[Occurrences] =
  	{
  		if (provOcc.isEmpty) List(List())
  		else {
  			val reccCombinations = iter(provOcc.tail) // Possible combinations of remaining letters
  			val firstElemList = firstElem(provOcc.head, List()) // Possible combinations of first Letter
  		
  			def combine(firstSide: List[Occurrences], secondSide: List[Occurrences]): List[Occurrences] =
  			{
  				
  				for{
  					a ← firstSide
  					b ← secondSide
  				} yield (a ++ b)
  				
  			}
  			combine(reccCombinations,firstElemList);
  		}
  		
  	}
  	def firstElem(pair: (Char, Int), firstEAcc: List[Occurrences]): List[Occurrences] =
  	{
  		if (pair._2 != 0) firstElem((pair._1, pair._2 - 1), List(List(pair)) ::: firstEAcc)
  		else List() :: firstEAcc
  	}
  	iter(occurrences)
  	
  }                                               //> combinations: (occurrences: forcomp.Anagrams.Occurrences)List[forcomp.Anagra
                                                  //| ms.Occurrences]
  def firstElem(pair: (Char, Int), firstEAcc: List[Occurrences]): List[Occurrences] =
  	{
  		if (pair._2 != 0) firstElem((pair._1, pair._2 - 1), List(List(pair)) ::: firstEAcc)
  		else List() :: firstEAcc
  	}                                         //> firstElem: (pair: (Char, Int), firstEAcc: List[forcomp.Anagrams.Occurrences
                                                  //| ])List[forcomp.Anagrams.Occurrences]
       combinations(List(('a', 2), ('b', 2)))     //> res0: List[forcomp.Anagrams.Occurrences] = List(List(), List((a,1)), List((
                                                  //| a,2)), List((b,1)), List((b,1), (a,1)), List((b,1), (a,2)), List((b,2)), Li
                                                  //| st((b,2), (a,1)), List((b,2), (a,2)))
	
	 
	//combine(List(List(),List(('a',1)), List(('a',2)),List(('a',3))),List(List(),List(('b',1))))
	
	
	
}