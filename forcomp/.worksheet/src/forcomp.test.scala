package forcomp
import Anagrams._
object test {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(929); 
def combinations(occurrences: Occurrences): List[Occurrences] =
  {
  	def iter(provOcc: Occurrences): List[Occurrences] =
  	{
  		if (provOcc.isEmpty) List(List())
  		else {
  			val reccCombinations = iter(provOcc.tail) // Possible combinations of remaining letters
  			val firstElemList = firstElem(provOcc.head, List()) // Possible combinations of first Letter
  		
  			def combine(firstSide: List[Occurrences], secondSide: List[Occurrences]): List[Occurrences] =
  			{
  				
  				for{
  					a ← firstSide
  					b ← secondSide
  				} yield (a ++ b)
  				
  			}
  			combine(reccCombinations,firstElemList);
  		}
  		
  	}
  	def firstElem(pair: (Char, Int), firstEAcc: List[Occurrences]): List[Occurrences] =
  	{
  		if (pair._2 != 0) firstElem((pair._1, pair._2 - 1), List(List(pair)) ::: firstEAcc)
  		else List() :: firstEAcc
  	}
  	iter(occurrences)
  	
  };System.out.println("""combinations: (occurrences: forcomp.Anagrams.Occurrences)List[forcomp.Anagrams.Occurrences]""");$skip(213); 
  def firstElem(pair: (Char, Int), firstEAcc: List[Occurrences]): List[Occurrences] =
  	{
  		if (pair._2 != 0) firstElem((pair._1, pair._2 - 1), List(List(pair)) ::: firstEAcc)
  		else List() :: firstEAcc
  	};System.out.println("""firstElem: (pair: (Char, Int), firstEAcc: List[forcomp.Anagrams.Occurrences])List[forcomp.Anagrams.Occurrences]""");$skip(46); val res$0 = 
       combinations(List(('a', 2), ('b', 2)));System.out.println("""res0: List[forcomp.Anagrams.Occurrences] = """ + $show(res$0))}
	
	 
	//combine(List(List(),List(('a',1)), List(('a',2)),List(('a',3))),List(List(),List(('b',1))))
	
	
	
}
