package interpreter

object Main extends App {
  import java.io.{BufferedReader, InputStreamReader}
  import Lisp._
  val in = new BufferedReader(new InputStreamReader(System.in))
  
  def repl {
    print("lisp> ")
    // TODO: Insert code for the REPL
    val s = in.readLine()

    try {
      println(lisp2string(evaluate(string2lisp(s))))
    }
    catch  {
      case e: Exception => println("Erreur : " + e.getMessage)
      case s: Error => println("Erreur : " + s.getMessage)
    }
    repl

  }

  repl
}

object LispCode {
  // TODO: implement the function `reverse` in Lisp.
  // From a list (a, b, c, d) it should compute (d, c, b, a)
  // Write it as a String, and test it in your REPL
  val reverse = """
  def (reverse L acc)
  (
     if (null? L)
        acc
        (reverse (cdr L) (cons (car L) acc))
  )
  """

  // TODO: implement the function `differences` in Lisp.
  // From a list (a, b, c, d ...) it should compute (a, b-a, c-b, d-c ...)
  // You might find useful to define an inner loop def

  val differences = """
  def (differences L)
  (
    def (sub l1 l2)
    (
      if (null? l1)
        nil
        (cons (- (car l1)(car l2))(sub (cdr l1)(cdr l2)))
    )
    (sub L (cons 0 L))
  )
  """

  val rebuildList = """
    def (rebuildList L)
    (
      def (it l1 l2)
      (
        if (null? l1)
          nil
          (cons (+ (car l1)(car l2)) (it(cdr l1) (cons (+ (car l1) (car l2)) (cdr (cdr l2)))))

      )

      (it L (cons 0 L))
    )
    """

  
  val withDifferences: String => String =
    (code: String) => "(" + reverse + " (" + differences + " (" + rebuildList + " " + code + ")))"
}
