object Midterm2006 {
  def suffixes[A](xs: List[A]): List[List[A]] = xs match {
  	case List() => List(xs)
  	case y :: ys => xs :: suffixes(ys)
  }                                               //> suffixes: [A](xs: List[A])List[List[A]]
  suffixes(List(2,3,5))                           //> res0: List[List[Int]] = List(List(2, 3, 5), List(3, 5), List(5), List())
   def prefixes[A](xs: List[A]): List[List[A]] = xs match {
  	case List() => List(xs)
  	case y :: ys => List() :: (prefixes(ys) map { x => y :: x})
  }                                               //> prefixes: [A](xs: List[A])List[List[A]]
  prefixes(List(2,3))                             //> res1: List[List[Int]] = List(List(), List(2), List(2, 3))
  
  type Queue[A] = Pair[List[A], List[A]]
	def append[A](q: Queue[A], x: A): Queue[A] = q match {
		case Pair(xs,ys) => Pair(xs, x::ys)
	}                                         //> append: [A](q: Midterm2006.Queue[A], x: A)Midterm2006.Queue[A]
	def head[A](q: Queue[A]): A = q match{
		case Pair(xs, ys) => xs.head
		case Pair(Nil, ys) => ys.reverse.head
	}                                         //> head: [A](q: Midterm2006.Queue[A])A
	def tail[A](q: Queue[A]): Queue[A] = q match{
		case Pair(xs, ys) => (xs.tail,ys)
		case Pair(Nil, ys) => (ys.reverse.tail, Nil)
	}                                         //> tail: [A](q: Midterm2006.Queue[A])Midterm2006.Queue[A]
  
  

}
case class FoodItem(name: String, price: Double, calories: Int, kind: Kind)

class Kind
{
	val Starter = new Kind()
	val MainCourse = new Kind()
	val Desert = new Kind()
	
	type Menu = Triple[FoodItem, FoodItem, FoodItem]
	
  def genMenus1(food: List[FoodItem], maxCalories: Int, maxPrice: Double) : List[Menu] =
  {
  	for{
  		s <- food
  		if (s.kind == Starter)
  		m <- food
  		if (m.kind == MainCourse)
  		d <- food
  		if (d.kind == Desert)
  		if (s.price + m.price + d.price <= maxPrice && s.calories + m.calories + d.calories <= maxCalories)
  	} yield (s,m,d)
  }
 
  
	
}