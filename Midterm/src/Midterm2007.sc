object Midterm2007 {
  abstract class Text{
  
		case class Chars(cs: List[Char]) extends Text
		case class Concat(t1: Text, t2: Text) extends Text
	
		def isEmpty: Boolean = this match {
			case Chars(cs) => cs.isEmpty
			case Concat(t1,t2) => t1.isEmpty && t2.isEmpty
		}
		def head: Char = this match {
			case Chars(cs) => cs.head
			case Concat(t1,t2) => if(t1.isEmpty) t2.head else t1.head
		}
		def tail: Text = this match {
			case Chars(cs) => Chars(cs.tail)
			case Concat(t1, t2) =>
				if (t1 isEmpty) t2.tail
				else { val t = t1.tail
							 if (t isEmpty) t2
							 else Concat(t, t2) }
}

		
  }
}