object Midterm2005 {
	type Row = List[Int];
	type Triangle = List[Row];

   def zipWith[A,B,C](xs: List[A], ys: List[B], f: (A,B) => C): List[C] =
   		if (xs.isEmpty || ys.isEmpty) Nil
   		else f(xs.head, ys.head) :: zipWith(xs.tail, ys.tail, f)
                                                  //> zipWith: [A, B, C](xs: List[A], ys: List[B], f: (A, B) => C)List[C]
   		
   		
   def nextRow(xs: List[Int]): List[Int] =
   		zipWith(0 :: xs, xs ::: List(0),(x: Int,y: Int) => x+y)
                                                  //> nextRow: (xs: List[Int])List[Int]
   		
   def pascal(x: Int): List[List[Int]] = {
   		def iter(c: Int, acc: List[List[Int]]): List[List[Int]] =
   			if (c == 0) acc
   			else iter(c-1, nextRow(acc.head) :: acc)
   		iter(x, List(List(1)))
   }                                              //> pascal: (x: Int)List[List[Int]]
   pascal(3)                                      //> res0: List[List[Int]] = List(List(1, 3, 3, 1), List(1, 2, 1), List(1, 1), Li
                                                  //| st(1))
   
}
	