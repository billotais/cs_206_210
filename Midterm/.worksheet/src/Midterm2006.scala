object Midterm2006 {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(148); 
  def suffixes[A](xs: List[A]): List[List[A]] = xs match {
  	case List() => List(xs)
  	case y :: ys => xs :: suffixes(ys)
  };System.out.println("""suffixes: [A](xs: List[A])List[List[A]]""");$skip(24); val res$0 = 
  suffixes(List(2,3,5));System.out.println("""res0: List[List[Int]] = """ + $show(res$0));$skip(154); 
   def prefixes[A](xs: List[A]): List[List[A]] = xs match {
  	case List() => List(xs)
  	case y :: ys => List() :: (prefixes(ys) map { x => y :: x})
  };System.out.println("""prefixes: [A](xs: List[A])List[List[A]]""");$skip(22); val res$1 = 
  prefixes(List(2,3))
  
  type Queue[A] = Pair[List[A], List[A]];System.out.println("""res1: List[List[Int]] = """ + $show(res$1));$skip(141); 
	def append[A](q: Queue[A], x: A): Queue[A] = q match {
		case Pair(xs,ys) => Pair(xs, x::ys)
	};System.out.println("""append: [A](q: Midterm2006.Queue[A], x: A)Midterm2006.Queue[A]""");$skip(114); 
	def head[A](q: Queue[A]): A = q match{
		case Pair(xs, ys) => xs.head
		case Pair(Nil, ys) => ys.reverse.head
	};System.out.println("""head: [A](q: Midterm2006.Queue[A])A""");$skip(133); 
	def tail[A](q: Queue[A]): Queue[A] = q match{
		case Pair(xs, ys) => (xs.tail,ys)
		case Pair(Nil, ys) => (ys.reverse.tail, Nil)
	};System.out.println("""tail: [A](q: Midterm2006.Queue[A])Midterm2006.Queue[A]""")}
  
  

}
case class FoodItem(name: String, price: Double, calories: Int, kind: Kind)

class Kind
{
	val Starter = new Kind()
	val MainCourse = new Kind()
	val Desert = new Kind()
	
	type Menu = Triple[FoodItem, FoodItem, FoodItem]
	
  def genMenus1(food: List[FoodItem], maxCalories: Int, maxPrice: Double) : List[Menu] =
  {
  	for{
  		s <- food
  		if (s.kind == Starter)
  		m <- food
  		if (m.kind == MainCourse)
  		d <- food
  		if (d.kind == Desert)
  		if (s.price + m.price + d.price <= maxPrice && s.calories + m.calories + d.calories <= maxCalories)
  	} yield (s,m,d)
  }
 
  
	
}
