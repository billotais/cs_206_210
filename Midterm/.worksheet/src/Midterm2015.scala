object Midterm2015 {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(109); 
  def differences(ls: List[Int]): List[Int] = (ls zip 0 :: ls) map {case (x,y) => x - y};System.out.println("""differences: (ls: List[Int])List[Int]""");$skip(39); val res$0 = 
  
  differences(List(1,-2,3,-4,5,-6));System.out.println("""res0: List[Int] = """ + $show(res$0));$skip(23); val res$1 = 
  differences(List(1))
  
  abstract class Tree
	case class Node(left: Tree, elem: Int, right: Tree) extends Tree
	case object Leaf extends Tree;System.out.println("""res1: List[Int] = """ + $show(res$1));$skip(718); 
  
   

  def computeMinMax(b: Node): (Int, Int) = b match {
		case Node(Leaf, k, Leaf) => (k, k)
	
		case Node(left: Node, k, Leaf) => computeMinMax(left) match {
			case (min, max) => (Math.min(min, k), Math.max(k, max))
		}
		case Node(Leaf, k, right: Node) => computeMinMax(right) match {
			case (min, max) => (Math.min(min, k), Math.max(k, max))
		}
		case Node(left: Node, k, right: Node) =>
			(computeMinMax(left), computeMinMax(right)) match {
				 case ((min1, max1),(min2, max2)) =>
					 (Math.min(Math.min(min1, k), min2), Math.max(Math.max(max1, k), max2))
				}
			}
  	
  	
  	};System.out.println("""computeMinMax: (b: Midterm2015.Node)(Int, Int)""")}
}
