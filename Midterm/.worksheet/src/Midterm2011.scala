object Midterm2011 {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(149); 
	def divisors(xs: List[Int]): List[(Int, Int)] =
  	for {
  		x <- xs
  		y <- xs
  		if (x != y && y % x == 0)
  	} yield(x, y);System.out.println("""divisors: (xs: List[Int])List[(Int, Int)]""");$skip(143); 
	
	def divisors2(xs: List[Int]): List[(Int, Int)] =
		(xs flatMap { x =>
			xs map {y => (x,y)}}).filter{case (x,y) => (x != y && y % x == 0)};System.out.println("""divisors2: (xs: List[Int])List[(Int, Int)]""");$skip(343); 
			
			
	def encode(xs: List[Char]): List[(Char, Int)] = {
		def iter(xs: List[Char], prov: List[(Char, Int)], c: Int): List[(Char, Int)] = xs match
		{
		case Nil => prov
		case z1::Nil => List((z1,1))
		case z1::z2::zs => if (z1 != z2) iter(z2::zs, (z1, c+1) :: prov, 0)
											 else iter(z2::zs, prov, c+1)
		}
		iter(xs, List(), 0)
	};System.out.println("""encode: (xs: List[Char])List[(Char, Int)]""");$skip(67); val res$0 = 
	encode(List('a','a','a','b','c','c','b','b','a','k','k','k','k'));System.out.println("""res0: List[(Char, Int)] = """ + $show(res$0))}
}
