object Midterm2005 {
	type Row = List[Int];
	type Triangle = List[Row];import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(247); ;

   def zipWith[A,B,C](xs: List[A], ys: List[B], f: (A,B) => C): List[C] =
   		if (xs.isEmpty || ys.isEmpty) Nil
   		else f(xs.head, ys.head) :: zipWith(xs.tail, ys.tail, f);System.out.println("""zipWith: [A, B, C](xs: List[A], ys: List[B], f: (A, B) => C)List[C]""");$skip(116); 
   		
   		
   def nextRow(xs: List[Int]): List[Int] =
   		zipWith(0 :: xs, xs ::: List(0),(x: Int,y: Int) => x+y);System.out.println("""nextRow: (xs: List[Int])List[Int]""");$skip(214); 
   		
   def pascal(x: Int): List[List[Int]] = {
   		def iter(c: Int, acc: List[List[Int]]): List[List[Int]] =
   			if (c == 0) acc
   			else iter(c-1, nextRow(acc.head) :: acc)
   		iter(x, List(List(1)))
   };System.out.println("""pascal: (x: Int)List[List[Int]]""");$skip(13); val res$0 = 
   pascal(3);System.out.println("""res0: List[List[Int]] = """ + $show(res$0))}
   
}
	