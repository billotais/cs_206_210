import scala.annotation.tailrec

object Midterm2014 {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(345); 
   def merge[T](as: List[T], bs: List[T])(cmp: (T, T) => Boolean): List[T] = (as, bs) match {
   	case (Nil, Nil) => Nil
   	case (xs, Nil) => xs
   	case (Nil, ys) => ys
   	case (x::xs, y::ys) => if (cmp(x,y)) x :: merge(xs, y::ys)(cmp)
   												 else y :: merge(x::xs, ys)(cmp)
   };System.out.println("""merge: [T](as: List[T], bs: List[T])(cmp: (T, T) => Boolean)List[T]""");$skip(428); 
   
   def merge2[T](as: List[T], bs: List[T])(cmp: (T, T) => Boolean): List[T] = {
   	def iter(l1: List[T], l2: List[T], acc: List[T]): List[T] = (l1, l2) match {
   		case (Nil, Nil) => acc
   		case (xs, Nil) => acc ::: xs
   		case (Nil, ys) => acc ::: ys
   		case (x::xs, y::ys) => if (cmp(x,y)) iter(xs, y::ys, acc ::: List(x))
   													 else iter(x::xs , ys, acc ::: List(y))
   	}
   	iter(as, bs, List())
   };System.out.println("""merge2: [T](as: List[T], bs: List[T])(cmp: (T, T) => Boolean)List[T]""");$skip(86); 
    
   def iterate[T](x: T)(f: T => T): Stream[T] = Stream.cons(x, iterate(f(x))(f));System.out.println("""iterate: [T](x: T)(f: T => T)Stream[T]""");$skip(173); 
   
   def iterated[T](f: T => T): Stream[T => T] = {
   	 def end(f: T => T): Stream[T => T] = Stream.cons(f, end((x: T) => f(f(x))))
   	 Stream.cons(y => y, end(f))
   };System.out.println("""iterated: [T](f: T => T)Stream[T => T]""");$skip(177); 
   
   def flatten(ls: List[Any]): List[Int] = ls match {
		case Nil => Nil
		case (x: Int) :: xs => x :: flatten(xs)
		case (x: List[Any]) :: xs => flatten(x) ++ flatten(xs)
};System.out.println("""flatten: (ls: List[Any])List[Int]""")}


}
