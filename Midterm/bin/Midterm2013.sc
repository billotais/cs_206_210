object Midterm2013 {
	case class Poly(ls: List[Int]){
	
		def + (that: Poly): Poly = Poly((ls zipAll (that.ls, 0, 0)) map {case(x,y) => x+y})
		def * (n: Double): Poly = Poly(ls map (x => x*n))
		def - (that: Poly): Poly = ls + (that * (-1))
	
	 
	}
	case class SparsesPoly(repr: List[(Int, Int)]) {
		def toSparse(p: Poly): SparsesPoly = SparsesPoly(p.ls.zipWithIndex.filter {case (x: Int, y: Int) => x != 0})
		def toDense(s: SparsesPoly): Poly = ???
	}
	
	def foldRight[T, Z](xs: List[T], z: Z, f: (T, Z) => Z): Z = xs match {
		case Nil => z
		case x :: xs => f(x, foldRight(xs, z, f))
	}
	def length[T](xs: List[T]): Int =
		foldRight(xs, 0, (x: T, acc: Int) => acc+1)
	
}