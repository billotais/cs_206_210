object Midterm2011 {
	def divisors(xs: List[Int]): List[(Int, Int)] =
  	for {
  		x <- xs
  		y <- xs
  		if (x != y && y % x == 0)
  	} yield(x, y)                             //> divisors: (xs: List[Int])List[(Int, Int)]
	
	def divisors2(xs: List[Int]): List[(Int, Int)] =
		(xs flatMap { x =>
			xs map {y => (x,y)}}).filter{case (x,y) => (x != y && y % x == 0)}
                                                  //> divisors2: (xs: List[Int])List[(Int, Int)]
			
			
	def encode(xs: List[Char]): List[(Char, Int)] = {
		def iter(xs: List[Char], prov: List[(Char, Int)], c: Int): List[(Char, Int)] = xs match
		{
		case Nil => prov
		case z1::Nil => List((z1,1))
		case z1::z2::zs => if (z1 != z2) iter(z2::zs, (z1, c+1) :: prov, 0)
											 else iter(z2::zs, prov, c+1)
		}
		iter(xs, List(), 0)
	}                                         //> encode: (xs: List[Char])List[(Char, Int)]
	encode(List('a','a','a','b','c','c','b','b','a','k','k','k','k'))
                                                  //> res0: List[(Char, Int)] = List((k,1))
}