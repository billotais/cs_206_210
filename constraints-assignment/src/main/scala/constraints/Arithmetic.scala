package constraints

import cafesat.api.FormulaBuilder._
import cafesat.api.Formulas._
import cafesat.api.Solver._

import scala.annotation.tailrec

/**
  * This object contains utility functions for encoding
  * some arithmetic constraints as boolean ones
  */
object Arithmetic {

  /**
    * Transforms a positive integer in binary form into its integer representation.
    * The `head` element of the input list contains the most
    * significant bit (the list is in big-endian form).
    */
  def binary2int(n: List[Boolean]): Int = {
    def iter(n: List[Boolean], pow: Int): Int = n match {
      case Nil => 0
      case x :: xs => if (x) pow + iter(xs, 2 * pow)
      else iter(xs, 2 * pow)
    }

    iter(n.reverse, 1)

  }

  /**
    * Encodes a positive integer number into base 2.
    * The `head` element of the resulting list contains the most significant
    * bit. This function should not return unnecessary leading zeros.
    */
  def int2binary(n: Int): List[Boolean] = {
    def iter(n: Int): List[Boolean] = {
      if (n == 0) List(false)
      else ((n % 2) == 1) :: iter(n / 2)
    }

    val prov = iter(n).reverse.dropWhile(!_)
    if (prov.isEmpty) false :: prov
    else prov
  }


  /**
    * This function takes two arguments, both representing positive
    * integers encoded in binary as lists of propositional formulas
    * (true for 1, false for 0). It returns
    * a formula that represents a boolean circuit that constraints
    * `n1` to be less than or equal to `n2`
    */
  def lessEquals(n1: List[Formula], n2: List[Formula]): Formula = {
    val t: Formula = true
    val f: Formula = false

    def iter(n: List[(Formula, Formula)]): Formula = n match {
      case Nil => t

      case (x, y) :: xs => ((x iff y) && iter(xs)) || (!x && y)
    }

    iter(n1.reverse.zipAll(n2.reverse, f, f).reverse)
  }

  /**
    * A full adder is a circuit that takes 3 one bit numbers, and returns the
    * result encoded over two bits: (cOut, s)
    */
  def fullAdder(a: Formula, b: Formula, cIn: Formula): (Formula, Formula) = {

    val s = !(!(a iff b) iff cIn)
    val cOut = (a && b) || (cIn && !(a iff b))
    (cOut, s)
  }

  /**
    * This function takes two arguments, both representing positive integers
    * encoded as lists of propositional variables. It returns a pair.
    *
    * The first element of the pair is a `List[Formula]`, and it represents
    * the resulting binary number.
    * The second element is a set of intermediate constraints that are created
    * along the way.
    *
    */
  def adder(n1: List[Formula], n2: List[Formula]): (List[Formula], Set[Formula]) = {
    def iter(n1: List[Formula], n2: List[Formula]): (List[Formula], Set[Formula]) = {
      val newC1 = propVar()
      val newC2 = propVar()

      (n1, n2) match {
        case (x :: Nil, y :: Nil) => {
          val (head, last) = fullAdder(x, y, false)
          (newC1 :: newC2 :: Nil, Set(head iff newC1, last iff newC2))
        }
        case (x :: xs, y :: ys) => {
          val (z :: zs, savedFormulas) = iter(xs, ys)
          val (head, second) = fullAdder(x, y, z)
          (newC1 :: newC2 :: zs, savedFormulas ++ Set(head iff newC1, second iff newC2))
        }
        case _ => sys.error("Unexpected case")
      }
    }

    val f: Formula = false
    val (n1Correct, n2Correct) = n1.reverse.zipAll(n2.reverse, f, f).reverse.unzip

    iter(n1Correct, n2Correct)
  }

  /**
    * A helper function that creates a less-equals formula
    * taking an integer and a formula as parameters
    */
  def lessEqualsConst(cst: Int, n: List[Formula]): Formula = {
    lessEquals(int2binary(cst), n)
  }

  /**
    * A helper function that creates a less-equals formula
    * taking a formula and an integer as parameters
    */
  def lessEqualsConst(n: List[Formula], cst: Int): Formula = {
    lessEquals(n, int2binary(cst))
  }


}
