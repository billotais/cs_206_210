package constraints

import cafesat.api.Formulas._
import cafesat.api.FormulaBuilder._
import cafesat.api.Solver._

/**
  * This component implements a constraint solver for assigning time slots to volunteers
  * for various tasks at a festival. A task may require more than one volunteer,
  * and a volunteer can take a limited number of tasks
  */
object Balelec {

  import Arithmetic._

  case class Volunteer(name: String) {
    override def toString = name
  }

  /**
    * A task is represented by its name and
    * its capacity, i.e. the exact number of people
    * required to complete it.
    */
  case class Task(name: String, capacity: Int) {
    override def toString = name
  }

  /**
    * This function schedules volunteers to tasks.
    * It takes as input a list of volunteers and a list of tasks.
    * The `availability` map contains mappings from volunteers to the
    * tasks they are available for.
    * A volunteer can be assigned to several tasks, but only
    * up to a maximum number of task specified by the `maxWorkload` parameter.
    * It is ok to not assign a volunteer to any task.
    *
    * The return value is a list of volunteers assigned to each task. The function only
    * returns a complete valid assignment, if no such assignment exists then the
    * function returns None.
    */
  def schedule(
                volunteers: List[Volunteer],
                tasks: List[Task],
                availability: Map[Volunteer, List[Task]],
                maxWorkload: Int
              ): Option[Map[Task, List[Volunteer]]] = {

    //generates one propositional variable per volunteer/task combination
    val propVariables: Map[(Volunteer, Task), PropVar] =
      volunteers.flatMap({ case v@Volunteer(name) =>
        tasks.map(s => (v, s) -> propVar(name))
      }).toMap

    /**
      * A Set of constraints that ensures that each volunteers
      * only get assigned to desired tasks
      *
      * => all propVariables (volunteer/task) representing non-avalible tasks are equal to false
      **/

    val possibleTasks: Seq[Formula] = volunteers map (v => {
      for {
        t <- tasks
        if (!availability(v).contains(t))
      } yield (!propVariables(v, t))
    }.foldLeft[Formula](true)(_ && _)
      )

    /**
      * A Set of constraints that ensures that each task
      * has enough volunteers assigned to
      */
    val enoughVolunteersPerTask: Seq[Formula] = tasks map (t => equals(t.capacity, {
      for {
        v <- volunteers
      } yield (propVariables(v, t))
    }))


    /**
      * A Set of constraints that ensures that each volunteers
      * doesn't has too much work
      */
    val notTooMuchWorkload: Seq[Formula] = volunteers map (v => atMostMaxTrue({
      for {
        t <- tasks
      } yield (propVariables(v, t))
    }, maxWorkload))

    // Combining all the constraints together

    val allConstraints: Seq[Formula] = possibleTasks ++ enoughVolunteersPerTask ++ notTooMuchWorkload

    // Finding a satisfying assignment to the constraints
    val res = solveForSatisfiability(and(allConstraints: _*))

    res.map(model => {
      tasks.map(t => {
        val assignedTask = volunteers.filter(v => model(propVariables((v, t))))
        (t, assignedTask)
      }).toMap
    })
  }

  /**
    * This function takes a list of constraint, and returns a
    * constraint that is true if and only if at most max
    * of them are true.
    */
  def atMostMaxTrue(ns: List[Formula], max: Int): Formula = {
    val (r, c) = countPositiveBits(ns)
    lessEquals(r, int2binary(max)) && and(c.toSeq: _*)
  }

  /**
    * This function takes a list of constraint, and returns a
    * constraint that is true if and only if at least min
    * of them are true.
    */
  def atLeastMinTrue(ns: List[Formula], min: Int): Formula = {
    val (r, c) = countPositiveBits(ns)
    lessEquals(int2binary(min), r) && and(c.toSeq: _*)
  }

  /**
    * This function takes a list of constraint, and returns a
    * constraint that is true if and only if n of them are true
    */
  def equals(n: Int, ns: List[Formula]): Formula = atLeastMinTrue(ns, n) && atMostMaxTrue(ns, n)


  /**
    * This function counts the number of positive bits in a number.
    *
    * It takes a list of formulas, and returns a pair.
    * The first element of the pair is a list of formulas representing the number
    * of ones in `ns`.
    * The second element is a set of additional constraints that have been gathered along
    * the way. Hint: see `adder` for understanding how to use additional constraints
    */
  def countPositiveBits(ns: List[Formula]): (List[Formula], Set[Formula]) = {
    ns.foldLeft((List[Formula](false), Set[Formula]())) { case ((tmpSum, tmpAcc), n) =>
      val (r, c) = adder(tmpSum, List(n))
      (r, tmpAcc ++ c)
    }
  }

}
