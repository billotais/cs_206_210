package pubsub.collection

class BoundedBuffer[T](size: Int) extends AbstractBoundedBuffer[T](size) {

  // You have at your disposition the following two variables:
  // - count : Int
  // - head : Int
  // In addition, you have access to an array-like internal buffer:
  // - buffer
  // You can access elements of this buffer using:
  // - buffer(i)
  // Similarly, you can set elements using:
  // - buffer(i) = e
  //
  // You do not need to create those variables yourself!
  // They are inherited from the AbstractBoundedBuffer class.




  def isEmpty(): Boolean = (count == 0)
  def isFull(): Boolean = (count >= buffer.size)
  var tail = 0
  override def put(e: T): Unit = this.synchronized {
    while (isFull()) wait()
    buffer.update(head, e)
    head += 1
    if (head >= buffer.size) head = 0
    count += 1
    notifyAll()
  }

  override def take(): T = this.synchronized {

    while (isEmpty()) wait()
    val v = buffer(tail)
    buffer.delete(tail)
    tail += 1
    if (tail >= buffer.size) tail = 0
    count -= 1
    notifyAll()
    v
  }







}
