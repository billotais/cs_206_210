package streams
import streams._



object testSheet {
	
	
	val level =	Bloxorz.Level1            //> level  : streams.Bloxorz.Level1.type = streams.Bloxorz$Level1$@3f3afe78
	val b1 = level.Pos(3,4)                   //> b1  : streams.testSheet.level.Pos = Pos(3,4)
	
	level.Block(b1, b1).legalNeighbors        //> res0: List[(streams.testSheet.level.Block, streams.testSheet.level.Move)] = 
                                                  //| List((Block(Pos(3,2),Pos(3,3)),Left), (Block(Pos(3,5),Pos(3,6)),Right), (Blo
                                                  //| ck(Pos(1,4),Pos(2,4)),Up))
  level.pathsFromStart.toSet                      //> java.lang.OutOfMemoryError: GC overhead limit exceeded
                                                  //| 	at scala.collection.immutable.Stream.$plus$plus(Stream.scala:372)
                                                  //| 	at scala.collection.immutable.Stream$$anonfun$$plus$plus$1.apply(Stream.
                                                  //| scala:372)
                                                  //| 	at scala.collection.immutable.Stream$$anonfun$$plus$plus$1.apply(Stream.
                                                  //| scala:372)
                                                  //| 	at scala.collection.immutable.Stream$Cons.tail(Stream.scala:1233)
                                                  //| 	at scala.collection.immutable.Stream$Cons.tail(Stream.scala:1223)
                                                  //| 	at scala.collection.immutable.Stream$$anonfun$append$1.apply(Stream.scal
                                                  //| a:255)
                                                  //| 	at scala.collection.immutable.Stream$$anonfun$append$1.apply(Stream.scal
                                                  //| a:255)
                                                  //| 	at scala.collection.immutable.Stream$Cons.tail(Stream.scala:1233)
                                                  //| 	at scala.collection.immutable.Stream$Cons.tail(Stream.scala:1223)
                                                  //| 	at scala.collection.generic.Growable$class.loop$1(Growable.scala:54)
                                                  //| 	at scala.collection.generic.Growable$class.$plus$plus$eq(Growable.scala:
                                                  //| 57)
                                                  //| 	at scala.collection.mutable.SetBuilder.$plus$
                                                  //| Output exceeds cutoff limit.
}