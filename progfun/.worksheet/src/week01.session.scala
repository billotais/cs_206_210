package week01

object session {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(41); val res$0 = 
  1 + 2;System.out.println("""res0: Int(3) = """ + $show(res$0));$skip(372); 
  def sqrt(x: Double) = {
    def abs(x: Double) = if (x < 0) -x else x
    
    def sqrtIter(guess: Double): Double =
      if (isGoodEnough(guess)) guess
      else sqrtIter(improve(guess))

    def isGoodEnough(guess: Double) =
      abs(guess * guess - x) / x < 0.001
      
    def improve(guess: Double) =
      (guess + x / guess) / 2
      
    sqrtIter(1.0)
  };System.out.println("""sqrt: (x: Double)Double""");$skip(17); val res$1 = 


  sqrt(1e-6);System.out.println("""res1: Double = """ + $show(res$1))}


	
	
	
	
}
