package week03

object rationals {
  val x = new Rational(1, 3)                      //> x  : week03.Rational = 1/3
  val y = new Rational(5, 7)                      //> y  : week03.Rational = 5/7
  val z = new Rational(3, 2)                      //> z  : week03.Rational = 3/2
  val a = new Rational(3)                         //> a  : week03.Rational = 3/1
  
  
  
  x - y - z                                       //> res0: week03.Rational = -79/42
  y + y                                           //> res1: week03.Rational = 10/7
  x < y                                           //> res2: Boolean = true
  x max y                                         //> res3: week03.Rational = 5/7
  
  
}

class Rational(x: Int, y: Int)
{
	require(y != 0, "Denominateur must be nonzero")
	
	def this(x: Int) = this(x, 1)

	private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
	
	def numer = x
	def denom = y
	
	def < (that: Rational) = numer * that.denom < that.numer * denom
		
	def max(that: Rational) = if(this < that) that else this
	
	def + (that: Rational) =
		new Rational(
		numer * that.denom + denom * that.numer,
		denom * that.denom)
		
	def unary_- =
		new Rational(-numer, denom)
		
	def - (that: Rational) = this + -that
	override def toString =
	{
		val g = gcd(numer, denom)
		numer / g + "/" + denom / g
	}
}