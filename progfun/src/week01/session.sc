package week01

object session {
  1 + 2                                           //> res0: Int(3) = 3
  def sqrt(x: Double) = {
    def abs(x: Double) = if (x < 0) -x else x
    
    def sqrtIter(guess: Double): Double =
      if (isGoodEnough(guess)) guess
      else sqrtIter(improve(guess))

    def isGoodEnough(guess: Double) =
      abs(guess * guess - x) / x < 0.001
      
    def improve(guess: Double) =
      (guess + x / guess) / 2
      
    sqrtIter(1.0)
  }                                               //> sqrt: (x: Double)Double


  sqrt(1e-6)                                      //> res1: Double = 0.0010000001533016628


	
	
	
	
}