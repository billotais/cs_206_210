package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

   lazy val genHeap: Gen[H] = for {
    e <- arbitrary[A]
    rest <- oneOf(Gen.const(empty), genHeap)
  } yield insert(e, rest)
  
  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }
  
 
  property("delFromSingle") = forAll {a: Int ⇒
  	val h = deleteMin(insert(a, empty))
  	isEmpty(h)
  }
  
  
  property("minOfHeap") = forAll { (a: H, b: H) ⇒
  	findMin(meld(a,b)) == Math.min(findMin(a),findMin(b))	
  	
  }
  
  
  property("isSorted") = forAll {h: H ⇒	
  	def isSorted(h: H): Boolean = {	
  		if (isEmpty(h)) true
  		else findMin(h) <= findMin(deleteMin(h)) && isSorted(deleteMin(h))
  	}
  	isSorted(h)	
  }
  
  property("meldCorrect") = forAll {(a: H, b: H) ⇒
  	def heapsEqual(c: H, d: H): Boolean = {
  		if (isEmpty(c) && isEmpty(d)) true
  		else if ((isEmpty(c) && ! isEmpty(d))|| (isEmpty(d) && !isEmpty(c))) false
  		else findMin(c) == findMin(d) && heapsEqual(deleteMin(c), deleteMin(d))
  	}
  	heapsEqual(meld(a,b), meld(insert(findMin(b),a), deleteMin(b)))
  } 	
}
